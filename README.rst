OIDF Customized xml2rfc
===========================
This repository hosts the version of xml2rfc-1.36 that is 
customized so that it will display OIDF specific 
boilerplates instead of that of IETF. 

The usage is the same as the stanrad xml2rfc-1.36 
but it has an extra "ipr" identified by the string "oidf". 

A sample document for such is provided in the 
  contrib/nat.sakimura/ 
folder. 

Enjoy, 

Nat Sakimura